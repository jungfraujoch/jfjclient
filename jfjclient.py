import string
from time import sleep

import openapi_client


ERROR_STATUS_WAITING = 504
STATUS_IDLE = "Idle"

# allow / to enable creation of subfolders
ALLOWED_CHARS = set(
    string.ascii_letters + string.digits + "_-+/"
)


def character_cleanup(s, default="_", allowed=ALLOWED_CHARS):
    return "".join(i if i in allowed else default for i in s)



class JFJClient:

    def __init__(self, host):
        configuration = openapi_client.Configuration(host=host)
        api_client = openapi_client.ApiClient(configuration)
        self.api = openapi_client.DefaultApi(api_client)

        self.actually_init()


    def actually_init(self):
        status = self.get_daq_status()
        if status != STATUS_IDLE:
            self.api.initialize_post()
            self.actually_wait_till_done()

        status = self.get_daq_status()
        if status != STATUS_IDLE:
            raise RuntimeError(f"status is not {STATUS_IDLE} but: {status}")


    def actually_wait_till_done(self):
        while True:
            try:
                done = self.api.wait_till_done_post()
            except Exception as e: #TODO: be more specific
                if e.status != ERROR_STATUS_WAITING:
                    print(e)
                    raise e
            else:
                #TODO: use number_of_triggers_left for progress...
                if done is None: # seems we get None instead of: status == 200
                    return
            sleep(0.1) #TODO


    def get_daq_status(self):
        return self.api.status_get().state

    def get_detector_status(self):
        return self.api.detector_status_get()#.state #TODO

    def get_detectors(self):
        return self.api.config_select_detector_get()

    def get_detector_config(self):
        return self.api.config_detector_get()


    def acquire(self, file_prefix, **kwargs):
        status = self.get_daq_status()
        if status != STATUS_IDLE:
            raise RuntimeError(f"status is not {STATUS_IDLE} but: {status}")

        file_prefix = character_cleanup(file_prefix)
        dataset_settings = openapi_client.DatasetSettings(file_prefix=file_prefix, **kwargs)

        self.api.start_post(dataset_settings=dataset_settings)
        self.actually_wait_till_done()


    def take_pedestal(self):
        self.api.pedestal_post()
        self.actually_wait_till_done()





if __name__ == "__main__":
    from datetime import datetime

    jfj = JFJClient("http://sf-daq-2:5232")

    dets = jfj.get_detectors()
    print(f"{dets=}")

    cfg = jfj.get_detector_config()
    print(f"{cfg=}")

    stat = jfj.get_detector_status()
    print(f"{stat=}")

    # jfj.take_pedestal()

    now = datetime.now().strftime("%Y%m%d-%H%M")
    name = f"test_{now}"
    jfj.acquire(
        beam_x_pxl = 1613,
        beam_y_pxl = 1666,
        detector_distance_mm = 151,
        incident_energy_keV = 12,
        sample_name = name,
        file_prefix = name,
        ntrigger = 10
    )



